package basic.android.weather;

import android.content.Context;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import static java.lang.Math.round;

public class StaticMethodsVariables {


    public static void toast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static String fahrenheitToCelsius(String f) {
        return round((float) (Integer.parseInt(f) - 32) / 1.8) + "°C";
    }

    public static void setWeatherIcon(Context mContext, String imgCode, ImageView imgIcon) {

        if (imgCode.equals("10")) {
            setImageWithGlide(mContext, "https://findicons.com/files/icons/1786/oxygen_refit/128/weather_showers.png", imgIcon);
        } else if (imgCode.equals("30")) {
          //  setImageWithGlide(mContext, "https://findicons.com/files/icons/1786/oxygen_refit/128/weather_few_clouds.png", imgIcon);
            setImageWithGlide(mContext, "https://findicons.com/files/icons/478/weather/128/sunny_period.png", imgIcon);
        } else if (imgCode.equals("31")) {
            setImageWithGlide(mContext, "https://findicons.com/files/icons/1035/human_o2/128/weather_clear_night.png", imgIcon);
        } else if (imgCode.equals("32")) {
            setImageWithGlide(mContext, "https://findicons.com/files/icons/1786/oxygen_refit/128/weather_clear.png", imgIcon);
        }
        /*else {
            setImageWithGlide(mContext, "https://findicons.com/files/icons/1035/human_o2/128/emblem_desktop_none.png", imgIcon);
        }
*/
    }

    public static void setImageWithGlide(Context mContext, String url, ImageView imgIcon) {
        Glide.with(mContext).load(url).into(imgIcon);
    }
}
