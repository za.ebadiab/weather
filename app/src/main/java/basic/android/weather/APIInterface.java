package basic.android.weather;


import basic.android.weather.IPAPIModel.IPAPICityModel;
import basic.android.weather.weatherModels.YahooWeatherModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface APIInterface {


    @GET("yql")
    Call<YahooWeatherModel> doGetYahooWeatherModels(@Query("q") String q, @Query("format") String format, @Query("env") String env);


    @GET("json")
    Call<IPAPICityModel> doGetCityNameFromIPAPI();

}
