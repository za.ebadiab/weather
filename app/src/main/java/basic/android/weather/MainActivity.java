package basic.android.weather;


import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.widget.Toast;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import java.net.URLEncoder;

import basic.android.weather.customViews.MyEditText;
import basic.android.weather.maps.MapsActivity;
import basic.android.weather.model.CityModel;
import basic.android.weather.weatherModels.YahooWeatherModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends BaseActivity {

    MyEditText txtEnterCityName;
    ForcastFragmentAdapter forcastFragmentAdapter;
    ViewPager pager;
    SmartTabLayout viewpagertabs;
    boolean cityExists = true;
    APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();
        setPagerAdapter();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setTabs();
            }
        }, 3000);


    }


    void bind() {

        txtEnterCityName = findViewById(R.id.txtEnterCityName);
        pager = findViewById(R.id.pager);
        viewpagertabs = findViewById(R.id.viewpagertabs);

        findViewById(R.id.btnSearch).setOnClickListener(vv -> {
            checkCityNameFromYahooWeather(txtEnterCityName.text());

        });

        findViewById(R.id.btnGoogleMap).setOnClickListener(vv -> {
            Intent intent = new Intent(mContext, MapsActivity.class);
            startActivity(intent);
        });

    }


    void setPagerAdapter() {
        forcastFragmentAdapter = new ForcastFragmentAdapter(getSupportFragmentManager(), mContext);
        pager.setAdapter(forcastFragmentAdapter);
        pager.setOffscreenPageLimit(3);

    }

    void setTabs() {
        viewpagertabs.setViewPager(pager);

    }


    void saveCityNameOnDB(String cityName) {
        CityModel cityModel = new CityModel();
        cityModel.setCityName(cityName);

        cityModel.save();

    }

    void checkCityNameFromYahooWeather(final String cityName) {
        apiInterface = APIClient.getClient("YahooWeatherModel").create(APIInterface.class);
        String q = "", format = "", env = "";
        try {
            q = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"" + cityName + " , ak\")";
            format = "json";
            env = URLEncoder.encode("store://datatables.org/alltableswithkeys", "UTF-8");

        } catch (Exception e) {

        }

        Call<YahooWeatherModel> call = apiInterface.doGetYahooWeatherModels(q, format, env);
        call.enqueue(new Callback<YahooWeatherModel>() {
            @Override
            public void onResponse(Call<YahooWeatherModel> call, Response<YahooWeatherModel> response) {

                checkYahooResponseForItemExists(response.body());

            }

            @Override
            public void onFailure(Call<YahooWeatherModel> call, Throwable t) {
                call.cancel();
            }
        });

    }

    void checkYahooResponseForItemExists(YahooWeatherModel yahooWeatherModel) {

        if (yahooWeatherModel.getQuery().getResults().getChannel().getTitle() == null) {
            cityExists = false;
            Toast.makeText(mContext, "City not exists!", Toast.LENGTH_LONG).show();

        } else {
            cityExists = true;
            saveCityNameOnDB(txtEnterCityName.text());
            setPagerAdapter();

            setTabs();
            pager.setCurrentItem(forcastFragmentAdapter.getCount());

        }
        txtEnterCityName.setText("");


    }


}

