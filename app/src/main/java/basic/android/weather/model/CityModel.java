package basic.android.weather.model;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;


public class CityModel extends SugarRecord {


    private String cityName;
    public CityModel() {
    }

    public CityModel(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
