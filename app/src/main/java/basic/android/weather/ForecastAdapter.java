package basic.android.weather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.List;

import basic.android.weather.customViews.MyTextView;
import basic.android.weather.weatherModels.Forecast;

import static basic.android.weather.StaticMethodsVariables.setWeatherIcon;

public class ForecastAdapter extends BaseAdapter {
    Context mContext;
    List<Forecast> forecastArrayList;

    public ForecastAdapter(Context mContext, List<Forecast> forecastArrayList) {
        this.mContext = mContext;
        this.forecastArrayList = forecastArrayList;
    }

    @Override
    public int getCount() {
        return forecastArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return forecastArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = LayoutInflater.from(mContext).inflate(R.layout.forecast_list_items, viewGroup, false);
        MyTextView txtDay = v.findViewById(R.id.txtDay);
        MyTextView txtMin = v.findViewById(R.id.txtMin);
        MyTextView txtText = v.findViewById(R.id.txtText);
        ImageView imgIcon = v.findViewById(R.id.imgIcon);

        txtDay.setText(forecastArrayList.get(i).getDay());
        txtMin.setText(forecastArrayList.get(i).getLow() + " - " + forecastArrayList.get(i).getHigh());
        txtText.setText(forecastArrayList.get(i).getText());
        setWeatherIcon(mContext, forecastArrayList.get(i).getCode(), imgIcon);

        return v;
    }


}
