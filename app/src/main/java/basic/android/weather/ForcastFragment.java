package basic.android.weather;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.gson.Gson;

import basic.android.weather.IPAPIModel.IPAPICityModel;
import basic.android.weather.weatherModels.Forecast;
import basic.android.weather.weatherModels.Query;


import java.net.URLEncoder;
import java.util.List;

import basic.android.weather.customViews.MyTextView;
import basic.android.weather.model.CityModel;
import basic.android.weather.weatherModels.YahooWeatherModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static basic.android.weather.StaticMethodsVariables.setWeatherIcon;
import static basic.android.weather.StaticMethodsVariables.toast;

public class ForcastFragment extends Fragment {

    MyTextView txtCity, txtTemp, txtConditionMinMax, txtText, txtCountry;
    ImageView imgIcon;
    ListView forecastListView;
    ProgressDialog progressDialog;
    static Context mContext;
    int mPosition;
    APIInterface apiInterface;


    private static ForcastFragment forcastFragment;


    public static ForcastFragment getFragment(int position, Context context) {
        mContext = context;
        //    if (bookFragment == null) {
        forcastFragment = new ForcastFragment();
        //  }

        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        forcastFragment.setArguments(bundle);

        return forcastFragment;

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.forcast_fragment_items, container, false);

        bind(v);
        int position = getArguments().getInt("position", 0);

        mPosition = position;

        if (position == 0) {
            getCityNameFromIPAPI();
        } else {
            getCityTempFromYahoo(getCityNameFromDB(position));
        }

        return v;
    }


    public String getCityNameFromDB(long i) {

        CityModel cityModel = CityModel.findById(CityModel.class, i + 1);

        if (cityModel != null) {
            return cityModel.getCityName();
        }
        return "";

    }


    void bind(View v) {
        imgIcon = v.findViewById(R.id.imgIcon);
        txtCity = v.findViewById(R.id.txtCity);
        txtCountry = v.findViewById(R.id.txtCountry);
        txtTemp = v.findViewById(R.id.txtTemp);
        txtConditionMinMax = v.findViewById(R.id.txtConditionMinMax);
        txtText = v.findViewById(R.id.txtText);
        forecastListView = v.findViewById(R.id.forecastListView);
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setTitle("loading...");

        progressDialog.setMessage("Please wait to load weather info from yahoo");


// This part use for listview scroll in a scrollView
        forecastListView.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

    }


    void getCityNameFromIPAPI() {

        apiInterface = APIClient.getClient("ForcastFragment.getCityNameFromIPAPI").create(APIInterface.class);


        Call<IPAPICityModel> call = apiInterface.doGetCityNameFromIPAPI();
        call.enqueue(new Callback<IPAPICityModel>() {
            @Override
            public void onResponse(Call<IPAPICityModel> call, Response<IPAPICityModel> response) {


                parseJsonIPAPISetCity(response.body());
            }

            @Override
            public void onFailure(Call<IPAPICityModel> call, Throwable t) {
                call.cancel();
            }
        });
    }

    /*   void getCityNameFromIPAPI() {
           AsyncHttpClient client = new AsyncHttpClient();
           client.get(StaticMethodsVariables.IP_API_URL, new TextHttpResponseHandler() {
               @Override
               public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                   toast(mContext, throwable.toString());
               }

               @Override
               public void onSuccess(int statusCode, Header[] headers, String responseString) {
                   parseJsonIPAPISetCity(responseString);
                   //  Hawk.put("currentCityJson", responseString);
               }
           });
       }
   */
    void parseJsonIPAPISetCity(IPAPICityModel iPAPICityModel) {
        try {

            String cityFromIPAPIJson = iPAPICityModel.getCity();

            saveCityNameOnDB(cityFromIPAPIJson);
            getCityTempFromYahoo(cityFromIPAPIJson);
        } catch (Exception e) {
            toast(mContext, "IPAPI data error!");
        }
    }


    void getCityTempFromYahoo(final String cityFromIPAPIJson) {

     /*   String Yahoo_Weather_URL = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"
                + cityFromIPAPIJson
                + "%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
*/

        progressDialog.show();
        apiInterface = APIClient.getClient("YahooWeatherModel").create(APIInterface.class);
        String q = "", format = "", env = "";
        try {
            q = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"" + cityFromIPAPIJson + " , ak\")";//.replace("%20", "%2B"), "UTF-8");
            format = "json";
            env = URLEncoder.encode("store://datatables.org/alltableswithkeys", "UTF-8");

        } catch (Exception e) {

        }

        Call<YahooWeatherModel> call = apiInterface.doGetYahooWeatherModels(q, format, env);
        call.enqueue(new Callback<YahooWeatherModel>() {
            @Override
            public void onResponse(Call<YahooWeatherModel> call, Response<YahooWeatherModel> response) {


                setYahooWeatherModelWithGSONSetTemp(response.body());
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<YahooWeatherModel> call, Throwable t) {
                call.cancel();
            }


        });


    }


    void setYahooWeatherModelWithGSONSetTemp(YahooWeatherModel yahooWeatherModel) {

        setWeatherIcon(mContext, yahooWeatherModel.getQuery().getResults().getChannel().getItem().getCondition().getCode(), imgIcon);

        String tempValue = yahooWeatherModel.getQuery().getResults().getChannel().getItem().getCondition().getTemp();
        txtTemp.setText(tempValue);

        txtCity.setText(yahooWeatherModel.getQuery().getResults().getChannel().getLocation().getCity());
        txtCountry.setText(yahooWeatherModel.getQuery().getResults().getChannel().getLocation().getCountry());

        txtText.setText(yahooWeatherModel.getQuery().getResults().getChannel().getItem().getCondition().getDate());
        List<Forecast> forecastArray = yahooWeatherModel.getQuery().getResults().getChannel().getItem().getForecast();

        txtConditionMinMax.setText(yahooWeatherModel.getQuery().getResults().getChannel().getItem().getCondition().getText() + "       " + forecastArray.get(0).getLow() + " - " + forecastArray.get(0).getHigh());

        forecastArray.remove(0);

        ForecastAdapter forecastAdapter = new ForecastAdapter(mContext, forecastArray);
        forecastListView.setAdapter(forecastAdapter);
    }


    void saveCityNameOnDB(String cityName) {
        CityModel cityModel;
        if (mPosition == 0) {
            cityModel = CityModel.findById(CityModel.class, (long) 1);
            if (cityModel == null) {
                cityModel = new CityModel();
            }
            cityModel.setCityName(cityName);
            cityModel.save();

        } else {
            cityModel = new CityModel();
            cityModel.setCityName(cityName);
            cityModel.save();
        }

    }


}

