package basic.android.weather;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static Retrofit retrofit = null;


    public static Retrofit getClient(String page) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        if (page.equals("YahooWeatherModel")) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://query.yahooapis.com/v1/public/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();

        } else if (page.equals("ForcastFragment.getCityNameFromIPAPI")) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://ip-api.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();

        }

        return retrofit;
    }

}
