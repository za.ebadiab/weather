package basic.android.weather;


import com.orhanobut.hawk.Hawk;
import com.orm.SugarApp;
import com.orm.SugarContext;


public class MainApplication extends SugarApp {


    @Override
    public void onCreate() {
        super.onCreate();

        Hawk.init(this).build();

        SugarContext.init(this);
    }

}


