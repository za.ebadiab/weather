package basic.android.weather.maps;

import android.content.DialogInterface;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.net.URLEncoder;
import java.util.List;

import basic.android.weather.APIClient;
import basic.android.weather.APIInterface;
import basic.android.weather.R;

import basic.android.weather.customViews.MyTextView;
import basic.android.weather.weatherModels.Forecast;
import basic.android.weather.weatherModels.YahooWeatherModel;
import cz.msebera.android.httpclient.Header;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static basic.android.weather.StaticMethodsVariables.setWeatherIcon;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

// http://blog.teamtreehouse.com/beginners-guide-location-android

    private GoogleApiClient googleApiClient;
    private GoogleMap mMap;
    private double longitude;
    private double latitude;

    private LocationRequest mLocationRequest;
    APIInterface apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

// Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds


        //Instantiating the GoogleApiClient
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


    }

    @Override
    protected void onResume() {
        super.onResume();
        googleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);

            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 9000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

// Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    }

    private void getCurrentLocation() {
        Log.d("my log:", "getCurrentLocation2");
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("my log:", "getCurrentLocation3");
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 300);
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            handleNewLocation(location);
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);
        }
    }

    private void handleNewLocation(Location location) {
        double currentLatitude = location.getLatitude();
        double currentLongitude = location.getLongitude();
        LatLng latLng = new LatLng(currentLatitude, currentLongitude);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setOnCameraIdleListener(this);
    }


    @Override
    public void onCameraIdle() {

        double lat = mMap.getCameraPosition().target.latitude;
        double lon = mMap.getCameraPosition().target.longitude;

        //true-   toast(this, lat + "," + lon);

        getWeatherFromYahooByLatLong(lat, lon);

    }

    void getWeatherFromYahooByLatLong(double latitude, double longitude) {

    /*    String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20text%3D%22("
         +latitude
          +"%2C"
         +longitude
         +")%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
*/

        apiInterface = APIClient.getClient("YahooWeatherModel").create(APIInterface.class);
        String q = "", format = "", env = "";
        try {
            q = "select * from weather.forecast where woeid in (select woeid from geo.places where text=\"(" + latitude + "," + longitude + ")\")";
            format = "json";
            env = URLEncoder.encode("store://datatables.org/alltableswithkeys", "UTF-8");

        } catch (Exception e) {

        }

        Call<YahooWeatherModel> call = apiInterface.doGetYahooWeatherModels(q, format, env);
        call.enqueue(new Callback<YahooWeatherModel>() {
            @Override
            public void onResponse(Call<YahooWeatherModel> call, Response<YahooWeatherModel> response) {


                setYahooWeatherModelWithGSONSetTemp(response.body());
            }

            @Override
            public void onFailure(Call<YahooWeatherModel> call, Throwable t) {
                call.cancel();
            }
        });

/*
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                           setYahooWeatherModelWithGSONSetTemp(responseString);
            }
        });
*/
    }

    void setYahooWeatherModelWithGSONSetTemp(YahooWeatherModel yahooWeatherModel) {
        //    Gson gson = new Gson();
        //   YahooWeatherModel yahooWeatherModel = gson.fromJson(responseString, YahooWeatherModel.class);
        String temp = yahooWeatherModel.getQuery().getResults().getChannel().getItem().getCondition().getTemp();

        String code = yahooWeatherModel.getQuery().getResults().getChannel().getItem().getCondition().getCode();


        String city = yahooWeatherModel.getQuery().getResults().getChannel().getLocation().getCity();
        String country = (yahooWeatherModel.getQuery().getResults().getChannel().getLocation().getCountry());

        List<Forecast> forecastArray = yahooWeatherModel.getQuery().getResults().getChannel().getItem().getForecast();
        String conditionMinMax = yahooWeatherModel.getQuery().getResults().getChannel().getItem().getCondition().getText() + "       " + forecastArray.get(0).getLow() + " - " + forecastArray.get(0).getHigh();

        String date = yahooWeatherModel.getQuery().getResults().getChannel().getItem().getCondition().getDate();

        showDialog(temp, code, city, country, conditionMinMax, date);


    }


    void showDialog(String temp, String code, String city, String country, String conditionMinMax, String date) {

        MyTextView txtCity, txtTemp, txtConditionMinMax, txtText, txtCountry;
        ImageView imgIcon;
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        View v = LayoutInflater.from(this).inflate(R.layout.weather_dialog, null, false);
        imgIcon = v.findViewById(R.id.imgIcon);
        txtCity = v.findViewById(R.id.txtCity);
        txtCountry = v.findViewById(R.id.txtCountry);
        txtTemp = v.findViewById(R.id.txtTemp);
        txtConditionMinMax = v.findViewById(R.id.txtConditionMinMax);
        txtText = v.findViewById(R.id.txtText);

        txtTemp.setText(temp);
        txtCity.setText(city);
        txtCountry.setText(country);
        txtConditionMinMax.setText(conditionMinMax);
        txtText.setText(date);

        setWeatherIcon(this, code, imgIcon);

        dialog.setView(v);

        dialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });


        dialog.show();

    }


    @Override
    public void onLocationChanged(Location location) {

        handleNewLocation(location);
    }


}
