package basic.android.weather;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


import basic.android.weather.model.CityModel;

public class ForcastFragmentAdapter extends FragmentStatePagerAdapter {
    Context mContext;

    public ForcastFragmentAdapter(FragmentManager fm, Context mContext) {
        super(fm);
        this.mContext = mContext;
    }

    @Override
    public Fragment getItem(int position) {
        return ForcastFragment.getFragment(position, mContext);

    }

    @Override
    public int getCount() {

        int count = (int) dbCount();
        if (count != 0) {
            return count;
        } else {
            return 1;
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return getCityNameFromDB(position);
    }

    long dbCount() {
        return CityModel.count(CityModel.class, null, null);
    }

    public String getCityNameFromDB(long i) {

        CityModel cityModel = CityModel.findById(CityModel.class, i + 1);

        if (cityModel != null) {
            return cityModel.getCityName();
        }
        return "";

    }

}

